

TBK & TbkDroid
--------------

TBK is a standalone pub accounting software, which strongly based on the customer's unique expectations.

Features:

 - Login system which allows only one user to log in until the end of the actual worksession.
 
 ![login](img/TBK/login.png)

 - Managable order of products in lists
 
 ![vételezés](img/TBK/vetelezes.png)
 
 - Comments in several tables, which contains usernames, can be protected with the containing user's password
 
 ![forgalom](img/TBK/forgalom.png)
 
 - Admin mode, to edit product and user properties
 
 ![admin](img/TBK/admin_dolg.png)
 
 - At the end of a worksession, a PDF file automatically generated with all the information about the actual worksession.
 
TbkDroid
--------------

TbkDroid is an android application. Its goal is to make easier the background work which means counting the products 
at the end of a worksession. With TbkDroid, the user can send all the data to the server (TBK).

 ![menu](img/TbkDroid/menu.png)
 
 - The application automatically recognize the TBK server so the user only have to type the password. This means that 
 the usercan only use this app when he/she logged in on the TBK server. (The login system uses zeronconf network)
 
  ![prodlist](img/TbkDroid/productlist.png)
 
 
